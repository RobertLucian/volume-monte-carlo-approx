# Approximating Volume in N-Dimensions w/ Monte Carlo

## Intro

In this little experiment, we are approximating the volume of the cube in *n* dimensions using the Monte Carlo approach.

What we are actually interested is in finding out if there is a pattern in the reported errors when the number of dimensions is increased and so is the magnitude of the basis vectors of the given cube.

## Setting Up the Environment

To set this up, make sure you've got `virtualenv` installed and run
```bash
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r requirements.txt
```
to install the dependencies and thento actually run the program do a
```
python run.py
```


## Approach

The way we are tackling this challenge is by constructing 2 cubes where one has a certain lateral length and the other one includes the first one while also doubling its length. 

This means that the higher the dimension, the greater is the ratio between the bigger volume and the smaller one - this rule is exponential and it increases this way *2 ^ number of dimensions*.

The reason we need to construct 2 cubes is that the Monte Carlo approach requires 2 quantities to compare after a certain number of random points have been generated. Obviously, the quantity is reflected in the number of points generated on either side.

Apart from getting a higher ratio between the 2 volumes whilst we go towards higher dimensions, also the data points become more disparate if a given fixed number of points is used in each scenario. 
If the random generator is poor, this could exarcerbate the error between the approximated volume of the cube and the actual one. The exarcerbation doesn't come from a lower density of these points, but from the fact that the volume ratio between the 2 cubes is ever-increasing, which leads to a poorer approximation due to a lower amount of points in the smaller cube - remember the `2 ^ number of dimensions` thing?

The only way to prevent getting poorer approximations due to the ever-increasing ratio of volumes between the 2 cubes as the number of dimensions increases is to keep the ratio of the volumes at a fixed value, such as 2, aka let one have double the volume of the other one.

This means that for vector basis scalers of *2, 1.41, 1.26, 1.19, 1.15* (with the inverses being *0.5, 0.71, 0.79, 0.84, 0.87*) and so on you'd need a chance of two 3rds for the point to fall between *0* and *50, 70, 79, 84, 87* % of the length of the bigger cube and another third between the complements of the previous percentages. This can be done by generating from a normal distribution a number from 0 to 1 and then from 0 to 0.5 scale it up to 0.8 and from 0.5 to 1.0 scale it from 0.8 to 1.0. For this, I would have to do two simpler linear interpolations.

Even in the current situation, we are pretty much safe from this loss of precision by not going into super high dimensions and by averaging multiple results in one test.

## Testing

To run this test, you're gonna have to run `python run.py` and make sure you've got Python 3. 

The 1st test is done with the following hyperparameters:
1. Number of Maximum Dimensions = 10
1. Maximum Magnitude of Each Basis Vector = 5
1. Scalar of the Vector = 2.0
1. Iterations = 50,0000
1. Number of Averages = 1

In the 2nd test I'm changing the number of iterations down to 50,000 and I increase the number of averages up to 10.

## Results

For the first test, I let it run for roughly 5-10 minutes and when it did finish, I got the following heatmap:
![](statics/500000.png)

The error can go from 0 up to 1, so in our graph, we generally see small errors across the whole spectrum, but it looks like the error tends to accumulate towards higher dimension cubes and also when the magnitude of the basis vector gets larger. These minor differences could also be caused by that dimensionality curse. This means that for a 10-dimensional cube, the volume of the bigger cube is *2^10=1024* larger than the smaller one - in the number of points, that translates to a mere 500000/1024 ~ 488 points associated for the smaller cube. 

That's why it is much better to distribute the points evenly by taking values from a distribution with a mean equal to *2 ^ (-dimension)*, provided the range of the generated values are between *[0, 1]*. You could end up generating far fewer points, like 1000 for each test, then average say the results from 10 tests and you still end up with a far better approximatation in higher dimensions whilst using far less resources (like 50:1).

Anyhow, the above test only ran once, meaning that it didn't get averaged, so when I ran it multiple times, this is the kind of averages I got.

![](statics/50000.gif)

As it can be seen, the trend is in getting higher errors the higher the dimension of the cube is and also when the basis vector's length gets bigger.

The next obvious move is averaging the results - for that case I used the hyperparameters for the 2nd test I have briefly mentioned above. This is what I got.

![](statics/avg_50000.png)

Now this looks much better already and it clearly shows how dimensionality is the curse of approximation of the cube with the Monte Carlo approach and also how the vector's length affects it. One thing to notice is that the basis vector's length issue is a linear problem, so just adding another amount of points is going to make the approximations that much better.