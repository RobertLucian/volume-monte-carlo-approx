import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

max_dimensions = 10
max_basis_vector_length = 3
iterations = 50000
basis_vector_scaler = 2.0
times = 10

def main():
    results = np.empty(shape=(max_dimensions, max_basis_vector_length))

    for t in range(times):
        for basis_vector in range(1, max_basis_vector_length + 1):
            for dimension in range(1, max_dimensions + 1):
                # basis_vector_scaler = np.power(2, 1 / dimension)
                # perc_size = 1.0 / basis_vector_scaler
                small_vectors = np.identity(dimension) * basis_vector
                big_vectors = np.identity(dimension) * basis_vector * basis_vector_scaler

                small_volume = np.linalg.det(small_vectors)
                big_volume = np.linalg.det(big_vectors)
                # volume_ratio = small_volume / big_volume

                count_within = 0.0
                count_outside = 0.0
                
                for i in range(iterations):
                    # generate new random n-dimensional point
                    nd_point = np.random.rand(dimension) * basis_vector * basis_vector_scaler
                    # and
                    # nd_point[nd_point <= 0.5] = np.interp(nd_point[nd_point <= 0.5], (0, 0.5), (0, perc_size))
                    # nd_point[nd_point > 0.5] = np.interp(nd_point[nd_point > 0.5], (0.5, 1.0), (perc_size, 1))
                    # nd_point *= basis_vector * basis_vector_scaler

                    # if the point falls within the smaller volume
                    if np.count_nonzero(nd_point <= basis_vector) == dimension:
                        count_within += 1
                    # it falls within the outside of the smaller volume
                    else:
                        count_outside += 1
                
                # calculate error and add it
                error = abs((small_volume - (count_within / (count_within + count_outside)) * big_volume) / small_volume)
                results[dimension - 1, basis_vector - 1] += error

    results /= times
    ax = plt.axes()
    sns.heatmap(
        results, 
        robust=True, 
        vmin=0.0, 
        annot=True, 
        ax = ax, 
        cmap = "YlGnBu",
        fmt=".2f",
        xticklabels=list(range(1, max_basis_vector_length + 1)),
        yticklabels=list(range(1, dimension + 1))
        )

    ax.set_xlabel('basis vector length')
    ax.set_ylabel('dimensions')
    plt.show()

if __name__ ==  "__main__":
    main()